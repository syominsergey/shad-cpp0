#pragma once

#include <array>
#include <cmath>
#include <iostream>
#include <initializer_list>
#include <algorithm>

class Vector {
public:
    Vector();
    Vector(std::initializer_list<double> list);
    Vector(std::array<double, 3> data);

    double& operator[](size_t ind);
    double operator[](size_t ind) const;

    Vector& operator-=(const Vector& rhs);
    Vector& operator+=(const Vector& rhs);

    Vector operator-() const;

    void Normalize();

    void Scale(double value);

    size_t Size() const;

    void Flip();

private:
    std::array<double, 3> data_;
};

inline Vector operator-(Vector lhs, const Vector& rhs);
inline Vector operator+(Vector lhs, const Vector& rhs);

inline Vector operator*(Vector lhs, double value);
inline Vector operator*(double value, Vector rhs);

inline Vector operator+(double value, Vector rhs);
inline Vector operator*(const Vector& lhs, const Vector& rhs);

inline double DotProduct(const Vector& lhs, const Vector& rhs);
inline Vector CrossProduct(const Vector& a, const Vector& b);
inline double Length(const Vector& vec);
