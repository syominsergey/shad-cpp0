#pragma once

#include <vector.h>

class Sphere {
public:
    Sphere(Vector center, double radius);
    const Vector& GetCenter() const;
    double GetRadius() const;
private:
    Vector center_;
    double radius_;
};

class Ray {
public:
    Ray(Vector origin, Vector direction);
    const Vector& GetOrigin() const;
    const Vector& GetDirection() const;
private:
    Vector origin_;
    Vector direction_;
};
